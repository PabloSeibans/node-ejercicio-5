const { response , request} = require('express')
const Datos = require('../data/data-date')

const dataDate = new Datos()

const dataGet = (req, res = response) => {

  let datos = dataDate.DataHora

  res.json({
    datos
  })
}


module.exports =  { dataGet }
