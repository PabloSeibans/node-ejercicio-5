const express = require('express');

class Server {

  constructor() {
    this.app = express();
    this.port = process.env.PORT || 8080;
    this.datePath = "/api/date"

    this.middlewares()
    this.routes();
  }

  middlewares() {
    this.app.use(express.json())
  }

  routes() {
    this.app.use(this.datePath, require('../routes/date'))

  }

  listen() {
    //Con process.env.PORT obtenemos el valor de la variable PORT que esta en el archivo .env
    this.app.listen(this.port, () => {
      console.log('Server running on the port', this.port);
    });
  }
  
}

module.exports = Server