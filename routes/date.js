const {Router} = require('express')

const {
  dataGet
} = require('../controllers/date')

const route = Router()
route.get('/', dataGet)

module.exports = route